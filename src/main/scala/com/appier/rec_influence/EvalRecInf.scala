package com.appier.rec_influence

import org.apache.log4j.LogManager
import org.apache.spark.storage.StorageLevel._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.SaveMode
import org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString

import org.rogach.scallop._

import org.apache.spark.sql.functions._
import org.apache.spark.sql._

import com.appier.sql.SQLBackend

object EvalRecInf {
  val logger = LogManager.getLogger(this.getClass.getSimpleName)

  val taskName = this.getClass.getSimpleName.init

  def main(args: Array[String]) {
        object conf extends ScallopConf(args) {
            val title = opt[String]()
            verify()
        }

        val optTitle = conf.title.toOption

        val title = (Option(taskName) ++ optTitle).mkString(" ")

        val sparkConf = JobUtil.defaultConf(title)
        val spark = SparkSession
            .builder()
            .config(sparkConf)
            .getOrCreate()
        import spark.implicits._

        spark.sparkContext.hadoopConfiguration.set("mapreduce.input.fileinputformat.input.dir.recursive","true")

        val imp_join_all2 = spark.read.parquet("s3a://appier-rosetta/imp_join_all/20191111/*/*")
        
        val imp_join_all2_2 = imp_join_all2
                .filter($"show_time".isNotNull)
                .filter($"click_time".isNotNull)
                .distinct()
                .select(
                    $"idfa", 
                    $"cmp_id", 
                    $"dynlnkg", 
                    $"partner_id", 
                    $"app_id", 
                    $"tagid", 
                    $"bundle_id", 
                    $"passed_user_list"
                )
        //lateral view explode(json2strarray(regexp_replace(bytesarray2str(passed_user_list), "\\[\"[\\w\\d-_]*\",\"\",?", "\\["))) as exploded_user_list
        imp_join_all2_2.registerTempTable("imp_join_all2_2")
        SQLBackend.register(spark.sqlContext, "SELECT idfa2str(idfa) as idfa, CID2OID(CMPID2STR(cmp_id)) as oid, bytes2str(dynlnkg) as dynlnkg, partnerid2str(partner_id) as partner_id, bytes2str(bundle_id) as bundle_id, bytes2str(app_id) as app_id, bytes2str(tagid) as tag_id, explode_passed_user_list as passed_user_list FROM imp_join_all2_2 lateral view explode(json2strarray(regexp_replace(bytesarray2str(passed_user_list), \"\\\\[\\\"[\\\\w\\\\d-_]*\\\",\\\"\\\",?\", \"\\\\[\"))) as explode_passed_user_list")
        val imp_join_all2_3 = spark.sql("SELECT idfa2str(idfa) as idfa, CID2OID(CMPID2STR(cmp_id)) as oid, bytes2str(dynlnkg) as dynlnkg, partnerid2str(partner_id) as partner_id, bytes2str(bundle_id) as bundle_id, bytes2str(app_id) as app_id, bytes2str(tagid) as tag_id, explode_passed_user_list as passed_user_list FROM imp_join_all2_2 lateral view explode(json2strarray(regexp_replace(bytesarray2str(passed_user_list), \"\\\\[\\\"[\\\\w\\\\d-_]*\\\",\\\"\\\",?\", \"\\\\[\"))) as explode_passed_user_list")
            .filter($"oid" === "tH7alO_rRpGAI6rMEZ22Dw")//coupang
            .select(
                $"idfa", 
                $"partner_id", 
                $"bundle_id", 
                //$"app_id", 
                //$"tag_id", 
                //udfRecAlgo($"dynlnkg") as "rec_algo", 
                //$"passed_user_list", 
                udfClickedProductID($"dynlnkg") as "clicked_product"
            )
            .filter($"clicked_product" !== "")
            .distinct() //one item only count once
            .select(
                $"idfa", 
                $"partner_id", 
                $"bundle_id", 
                //$"app_id", 
                //$"tag_id", 
                //$"rec_algo", 
                //$"passed_user_list", 
                $"clicked_product" as "item_id"
            )
            .cache()
        
        val click_count = imp_join_all2_3
            //.groupBy($"partner_id")
            .groupBy($"partner_id", $"bundle_id")
            //.groupBy($"app_id")
            //.groupBy($"tag_id")
            //.groupBy($"rec_algo")
            //.groupBy($"passed_user_list")
            .count()
            .select(
                $"partner_id", 
                $"bundle_id", 
                //$"app_id", 
                //$"tag_id", 
                //$"rec_algo", 
                //$"passed_user_list", 
                $"count" as "click_count"
            )

        val retargeting_streaming = spark.read.parquet("s3a://retargeting-spark/retargeting-data/d_src=streaming_3.0.0/d_etlver=0.0.0/d_day=20191111/*/*")

        val retargeting_streaming_2 = retargeting_streaming
            .filter($"site_id" === "android--com.coupang.mobile")//coupang
            .filter($"event" !== "3rdParty_IDFA")
            .filter($"event" === "type_product")
            //.filter($"event" === "type_purchase")
            .filter($"utm_source" === "Appier")
            .select(
                $"user_ids".getItem("idfa") as "idfa", 
                //$"type_purchase_item" as "type_purchase_items"//Deprecated and migrated in document, but actually no 
                $"type_product_item"
            )
            .filter($"idfa".isNotNull) //Some Idfa is null
            .select(
                $"idfa", 
                //explode($"type_purchase_items") as "type_purchase_item"
                $"type_product_item"
            )
            .distinct() //one item only count once
            .select(
                $"idfa", 
                //$"type_purchase_item" as "item_id"
                $"type_product_item" as "item_id"
            )

        val event_count = imp_join_all2_3
            .join(retargeting_streaming_2, Seq("idfa", "item_id"))
            //.groupBy($"partner_id")
            .groupBy($"partner_id", $"bundle_id")
            //.groupBy($"app_id")
            //.groupBy($"tag_id")
            //.groupBy($"passed_user_list")
            .count()
            .select(
                $"partner_id", 
                $"bundle_id", 
                //$"app_id", 
                //$"tag_id", 
                //$"rec_algo", 
                //$"passed_user_list", 
                $"count" as "event_count"
            )

        event_count
            //.join(click_count, Seq("partner_id"))
            .join(click_count, Seq("partner_id", "bundle_id"))
            //.join(click_count, Seq("app_id"))
            //.join(click_count, Seq("tag_id"))
            //.join(click_count, Seq("rec_algo"))
            //.join(click_count, Seq("passed_user_list"))
            .select(
                $"partner_id", 
                $"bundle_id", 
                //$"app_id", 
                //$"tag_id", 
                //$"rec_algo", 
                //$"passed_user_list", 
                $"event_count" / $"click_count" as "have_same_item_type_product", 
                //$"event_count" / $"click_count" as "have_same_item_type_purchase", 
                $"click_count"
            )
            .orderBy($"click_count".desc)
            .show(200, false)
    }

    val udfByteToStr = udf(byteToStr _)
    def byteToStr(bs: Array[Byte]): String = {
        encodeBase64URLSafeString(bs)
    }

    val udfClickedProductID = udf(clickedProductID _)
    def clickedProductID(dynlnkg: String): String = {
        val dynlnkg_split = dynlnkg.split("~")
        if (dynlnkg_split.size == 3){
            return dynlnkg_split(2)
        }
        else{
            return ""
        }
    }

    val udfRecAlgo = udf(recAlgo _)
    def recAlgo(dynlnkg: String): String = {
        val dynlnkg_split = dynlnkg.split("~")
        if (dynlnkg_split.size == 3){
            return dynlnkg_split(1)
        }
        else{
            return ""
        }
    }
}
